import PageBackground from "./pagebackground.js";
import PageContent from "./pagecontent.js";

export default class Page extends paper.Rectangle {
    #number;
    #background;
    #content;
    constructor({ number, top, left, template, contentJSON }) {
        const { width, height, ...backgroundDescriptor } = template;
        super(left, top, width, height);
        this.#number = number;
        this.#background = new PageBackground(this, backgroundDescriptor);
        this.#content = new PageContent(this, contentJSON);
    }
    get number() {
        return this.#number;
    }
    get content() {
        return this.#content;
    }
    get template() {
        return {
            width: this.width,
            height: this.height,
            ...this.#background.descriptor,
        };
    }
    set template(newTemplate) {
        this.#background.set(newTemplate);
    }
    serialize({ contentJSON, contentSVG, rulingJSON, rulingSVG }) {
        return {
            number: this.#number,
            top: this.top,
            left: this.left,
            template: this.template,
            ...(contentJSON ? { contentJSON: this.#content.exportJSON() } : {}),
            ...(contentSVG ? { contentSVG: this.#content.exportSVG() } : {}),
            ...(rulingJSON
                ? { rulingJSON: this.#background.exportJSON() }
                : {}),
            ...(rulingSVG ? { rulingSVG: this.#background.exportSVG() } : {}),
        };
    }
    async updateAllCaches() {
        await this.#background.updateCache();
        await this.#content.updateCache();
    }
}
