import { distance } from "../../mathutils.js";
import GeometryTool from "../abstracttools/geometrytool.js";

export default class CircleTool extends GeometryTool {
    constructor() {
        const createCircle = (point) => {
            this.shape = new paper.Path.Circle(point, 0);
        };

        const updateCircle = (point, origin) => {
            const radius = distance(origin, point);
            this.shape.remove();
            this.shape = new paper.Path.Circle(origin, radius);
        };

        const handlers = {
            createShape: createCircle,
            updateShape: updateCircle,
        };
        super("circle", handlers);
    }
}
