import { $each, hide, show } from "../gui/dom-utils.js";
import DrawingTool from "./abstracttools/drawingtool.js";
import GeometryTool from "./abstracttools/geometrytool.js";
import CircleTool from "./concretetools/circletool.js";
import EraseTool from "./concretetools/erasetool.js";
import FreeLineTool from "./concretetools/freelinetool.js";
import MoveItemsTool from "./concretetools/moveitemstool.js";
import RectangleTool from "./concretetools/rectangletool.js";
import StraightLineTool from "./concretetools/straightlinetool.js";

const freeLineTool = new FreeLineTool();
const straightLineTool = new StraightLineTool();
const rectangleTool = new RectangleTool();
const circleTool = new CircleTool();
const eraseTool = new EraseTool();
const moveItemsTool = new MoveItemsTool();

const tools = [
    freeLineTool,
    straightLineTool,
    rectangleTool,
    circleTool,
    eraseTool,
    moveItemsTool,
];

const toolStack = Object.preventExtensions({
    tools,
    _activeTool: undefined,
    _previous: undefined,
    set activeTool(name) {
        this._previous = this.activeTool;
        const tool = this.get(name);
        tool.activate();
        this._activeTool = name;
        $each(".toolbarModule", (module) => {
            const moduleIsRequired = module.classList.contains(
                `toolbarModule-${tool.type}`
            );
            if (moduleIsRequired) show(module);
            else hide(module);
        });
    },
    get activeTool() {
        return this._activeTool;
    },
    get(name) {
        return this.tools.find((tool) => tool.name === name);
    },
    activatePrevious() {
        this.activeTool = this._previous;
    },

    /**
     * @param {{ strokeColor?: string; strokeWidth?: number; }} newStyle
     */
    setDrawingStyle(newStyle) {
        DrawingTool.style = { ...DrawingTool.style, ...newStyle };
        moveItemsTool.setStyleOfSelection(newStyle);
    },
    /**
     * @param {boolean} snapsToGrid
     */
    set snapsToGrid(snapsToGrid) {
        GeometryTool.snapsToGrid = snapsToGrid;
    },
    /**
     * @param {string} selectionMode
     */
    set selectionMode(selectionMode) {
        MoveItemsTool.selectionMode = selectionMode;
    },
});

toolStack.activeTool = "freeLine";

export default toolStack;
