import toolStack from "../../tools/toolstack.js";
import { $, $each } from "../dom-utils.js";
import { addColorOption } from "../gui-actions.js";

$each("input[name='radGrp-shape']", (input) => {
    const drawingShape = input.value;
    input.addEventListener("input", function () {
        toolStack.activeTool = drawingShape;
    });
});

$("input#slider-strokeWidth").addEventListener("input", function () {
    toolStack.setDrawingStyle({ strokeWidth: this.value });
});

$("input#switch-snapToGrid").addEventListener("input", function () {
    toolStack.snapsToGrid = this.checked;
});

$each("input[name='radGrp-eraser']", (button) => {
    /* eraser setting not yet implemented */
});

$each("input[name='radGrp-select']", (button) => {
    const selectionMode = button.value;
    button.addEventListener("input", () => {
        toolStack.selectionMode = selectionMode;
    });
});

export default function initToolbar() {
    addColorOption("#000000"); // black
    addColorOption("#db3535"); // red
    addColorOption("#4ec33d"); // green
    addColorOption("#00a0ec"); // blue

    const firstColorOption = $("#toolbarModule-strokeColor input");
    const strokeWidthSlider = $("input#slider-strokeWidth");

    firstColorOption.checked = true;
    toolStack.setDrawingStyle({
        strokeColor: firstColorOption.value,
        strokeWidth: strokeWidthSlider.value,
    });
}
