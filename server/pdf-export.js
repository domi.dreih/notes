const PDFKitDocument = require("pdfkit");
const { PDFDocument: PDFLibDocument } = require("pdf-lib");
const blobStream = require("blob-stream");
const { createWriteStream } = require("fs");
const SVGtoPDF = require("svg-to-pdfkit");
const { dialog } = require("electron");
const { writeFile } = require("fs/promises");

module.exports.exportPDF = async (file, options, win) => {
    const hasPDFBackground =
        options.includePDFBackground &&
        file.pages.reduce(
            (acc, cur) => acc || cur.template.pdfBackground !== null,
            false
        );
    let path = dialog.showSaveDialogSync(win, {
        title: "PDF-Datei exportieren",
        properties: ["showOverwriteConfirmation"],
        filters: [
            {
                name: "PDF-Dateien",
                extensions: ["pdf"],
            },
            {
                name: "Alle Dateien",
                extensions: ["*"],
            },
        ],
    });
    if (path === undefined || path.length === 0) return;
    if (!path.endsWith(".pdf")) path += ".pdf";

    const pdfDoc = new PDFKitDocument({ autoFirstPage: false });
    const stream = pdfDoc.pipe(
        hasPDFBackground ? blobStream() : createWriteStream(path)
    );

    for (const page of file.pages) {
        const includeRuling =
            options.includeRuling && page.template.pdfBackground === null;
        exportPage(page, includeRuling);
    }
    if (!hasPDFBackground) {
        pdfDoc.end();
        return;
    }

    const loadedBackgroundPDFs = {};
    for (const fingerprint in file.pdfData) {
        if (!Object.hasOwnProperty.call(file.pdfData, fingerprint)) continue;
        const pdfData = file.pdfData[fingerprint];
        loadedBackgroundPDFs[fingerprint] = await PDFLibDocument.load(pdfData);
    }

    stream.on("finish", async () => {
        const contentBuffer = await stream.toBlob().arrayBuffer();
        const contentPDF = await PDFLibDocument.load(contentBuffer);
        const outputPDF = await PDFLibDocument.create();

        for (let i = 0; i < contentPDF.getPageCount(); i++) {
            const { template } = file.pages[i];
            if (template.pdfBackground !== null) {
                const { pageNumber, fingerprint } = template.pdfBackground;
                const backgroundPDF = loadedBackgroundPDFs[fingerprint];
                const addedPage = await addPage(backgroundPDF, pageNumber - 1);
                drawPage(contentPDF, i, addedPage);
            } else {
                await addPage(contentPDF, i);
            }
        }
        const outputData = await outputPDF.save();
        await writeFile(path, outputData);

        async function addPage(sourcePDF, index) {
            const [copiedPage] = await outputPDF.copyPages(sourcePDF, [index]);
            outputPDF.addPage(copiedPage);
            return copiedPage;
        }
        async function drawPage(sourcePDF, index, destinationPage) {
            const sourcePage = sourcePDF.getPage(index);
            const embeddedPage = await outputPDF.embedPage(sourcePage);
            destinationPage.drawPage(embeddedPage);
        }
    });
    pdfDoc.end();

    function exportPage({ template, contentSVG, rulingSVG }, includeRuling) {
        pdfDoc.addPage({ size: [template.width, template.height] });
        if (includeRuling)
            SVGtoPDF(pdfDoc, rulingSVG, 0, 0, { assumePt: true });
        SVGtoPDF(pdfDoc, contentSVG, 0, 0, { assumePt: true });
    }
};
