import { resizeScopes } from "../docview/papersetup.js";
import { addPage } from "../pages/pagemanager.js";
import { $, hide } from "./dom-utils.js";
import "./eventlisteners/exportmenu.js";
import "./eventlisteners/filesetupmenu.js";
import initMenubar from "./eventlisteners/menubar.js";
import "./eventlisteners/pagesetupmenu.js";
import initToolbar from "./eventlisteners/toolbar.js";
import { adjustSubmenuPosition } from "./gui-actions.js";
import { initLang } from "./languages.js";

window.addEventListener("resize", function () {
    const main = $("#main");
    resizeScopes(main.offsetWidth, main.offsetHeight);
    adjustSubmenuPosition();
});

$("#btn-addPage").addEventListener("click", function () {
    hide(this);
    addPage();
});

export default function initGUI() {
    initLang();
    initToolbar();
    initMenubar();
}
