import { fileEventHandlers } from "../../file.js";
import history from "../../history/history.js";
import { appendPDF } from "../../pdf-insertion.js";
import { insertImage } from "../../tools/imagehandler.js";
import toolStack from "../../tools/toolstack.js";
import { $, $each } from "../dom-utils.js";
import {
    adjustSubmenuPosition,
    setVersionInfo,
    toggleOverlay,
} from "../gui-actions.js";

$each("input[name='radGrp-overlay']", (input) => {
    const overlayID = input.value;
    input.addEventListener("click", () => {
        toggleOverlay(overlayID);
    });
});

function openNewFileSetup() {
    toggleOverlay("newFileSetup");
}

$("#btn-new").addEventListener("click", async () => {
    toggleOverlay("file");
    const continues = await fileEventHandlers.newFile();
    if (continues) openNewFileSetup();
});

$("#btn-open").addEventListener("click", () => {
    toggleOverlay("file");
    fileEventHandlers.openFile();
});

$("#btn-save").addEventListener("click", () => {
    toggleOverlay("file");
    fileEventHandlers.saveFile();
});

$("#btn-saveAs").addEventListener("click", () => {
    toggleOverlay("file");
    fileEventHandlers.saveFileAs();
});

$("#btn-export").addEventListener("click", () => {
    toggleOverlay("export");
});

$("#btn-insertImage").addEventListener("click", () => {
    toggleOverlay("insert");
    insertImage();
});

$("#btn-insertPDF").addEventListener("click", () => {
    toggleOverlay("insert");
    appendPDF();
});

$each("input[name='radGrp-mode']", (input) => {
    const mode = input.value;
    input.addEventListener("click", () => {
        if (mode === "draw") {
            const activeShape = $("input[name='radGrp-shape']:checked").value;
            toolStack.activeTool = activeShape;
            return;
        }
        toolStack.activeTool = mode;
    });
});

$("#btn-undo").addEventListener("click", () => history.undo());
$("#btn-redo").addEventListener("click", () => history.redo());

export default function initMenubar() {
    setVersionInfo();
    adjustSubmenuPosition();
}
