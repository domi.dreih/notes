import Modification from "./modification.js";

export default class BulkModification extends Modification {
    constructor(modifications) {
        const undo = () =>
            modifications.forEach((modification) => modification.undo());
        const redo = () =>
            modifications.forEach((modification) => modification.redo());
        super({ undo, redo });
    }
}
