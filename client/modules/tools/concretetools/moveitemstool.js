import { $ } from "../../gui/dom-utils.js";
import { getPageFromPoint } from "../../pages/pagemanager.js";
import BulkEditingTool from "../abstracttools/bulkeditingtool.js";

export default class MoveItemsTool extends BulkEditingTool {
    static selectionStyle = {
        strokeColor: "#5870cb",
        strokeWidth: 1.5,
        strokeCap: "round",
        strokeJoin: "round",
        strokeScaling: false,
        dashArray: [8, 4],
    };
    static selectionMode = "rect";
    #selectionPath;
    #origin;
    #selectedGroup;
    mode = "select"; // either "select", "move" or "resize"
    #selectedItems = [];
    #resizeOption;

    constructor() {
        const onMouseMove = (event) => this.updateCursor(event.point);

        const onMouseDown = (event) => {
            this.#origin = event.point;
            this.#resizeOption = MoveItemsTool.getResizeOption(
                this.#origin,
                this.#selectedGroup
            );
            this.mode = this.determineMode();
            if (this.mode !== "select") return;

            this.#selectedGroup?.selected &&
                (this.#selectedGroup.selected = false);
            this.#selectionPath = MoveItemsTool.#usesLassoSelect
                ? new paper.Path()
                : new paper.Path.Rectangle(this.#origin, this.#origin);
            this.#selectionPath.style = MoveItemsTool.selectionStyle;
        };

        const onMouseDrag = (event) => {
            switch (this.mode) {
                case "select":
                    if (MoveItemsTool.#usesLassoSelect) {
                        this.#selectionPath.add(event.point);
                        this.#selectionPath.closePath();
                        break;
                    }
                    this.#selectionPath.remove();
                    this.#selectionPath = new paper.Path.Rectangle(
                        this.#origin,
                        event.point
                    );
                    this.#selectionPath.style = MoveItemsTool.selectionStyle;
                    break;
                case "move":
                    this.#selectedGroup.translate(
                        event.point.subtract(this.#origin)
                    );
                    this.#origin = event.point;
                    break;
                case "resize":
                    const origin = this.#resizeOption.oppositePoint;
                    const desiredSize = event.point.getDistance(origin);
                    const { bounds } =
                        this.#selectedGroup.children.selectionOutline;
                    const currentSize = bounds.topLeft.getDistance(
                        bounds.bottomRight
                    );
                    const factor = desiredSize / currentSize;
                    this.#selectedGroup.scale(factor, origin);
                    break;
                default:
                    break;
            }
        };

        const onMouseUp = (event) => {
            if (this.mode === "move") {
                this.#managePageTransfer(event.point);
            }
            if (this.mode !== "select") {
                this.#finishModification();
                return;
            }
            this.page.content.deselectAll(false);
            this.createCache();
            MoveItemsTool.#usesLassoSelect && this.#selectionPath.closePath();
            const allItems = this.page.content.group.children;
            this.#selectedItems = MoveItemsTool.#usesLassoSelect
                ? MoveItemsTool.lassoSelect(allItems, this.#selectionPath)
                : MoveItemsTool.rectangleSelect(allItems, this.#selectionPath);
            this.#selectedGroup = this.page.content.select(this.#selectedItems);
            this.#selectionPath.remove();
            this.updateCursor(event.point);
        };

        const onDeactivate = () => {
            this.page.content.deselectAll();
            this.#selectedItems = [];
            this.updateCursor();
        };

        const handlers = {
            onMouseMove,
            onMouseDown,
            onMouseDrag,
            onMouseUp,
            onDeactivate,
        };
        super("moveItems", "moveItems", handlers);
    }

    determineMode() {
        if (typeof this.#selectedGroup !== "object") return "select";
        if (this.#resizeOption.resizePossible) return "resize";
        if (this.#selectedGroup.contains(this.#origin)) return "move";
        return "select";
    }

    static get #usesLassoSelect() {
        return MoveItemsTool.selectionMode === "lasso";
    }

    static getResizeOption(point, selection) {
        const defaultOption = {
            resizePossible: false,
            cursor: $("#main").style.cursor,
        };
        if (!(point?.x && point?.y)) return defaultOption;
        if (typeof selection !== "object") return defaultOption;
        const bounds = selection.bounds;
        const hitTolerance = 5;
        if (point.getDistance(bounds.bottomRight) < hitTolerance)
            return {
                resizePossible: true,
                selectedPoint: bounds.bottomRight,
                oppositePoint: bounds.topLeft,
                cursor: "nw-resize",
            };
        if (point.getDistance(bounds.topLeft) < hitTolerance)
            return {
                resizePossible: true,
                selectedPoint: bounds.topLeft,
                oppositePoint: bounds.bottomRight,
                cursor: "nw-resize",
            };
        if (point.getDistance(bounds.bottomLeft) < hitTolerance)
            return {
                resizePossible: true,
                selectedPoint: bounds.bottomLeft,
                oppositePoint: bounds.topRight,
                cursor: "ne-resize",
            };
        if (point.getDistance(bounds.topRight) < hitTolerance)
            return {
                resizePossible: true,
                selectedPoint: bounds.topRight,
                oppositePoint: bounds.bottomLeft,
                cursor: "ne-resize",
            };
        return defaultOption;
    }

    static rectangleSelect(allItems, rectanglePath) {
        return allItems.filter((item) => {
            if (item.name === "clipMask") return false;
            if (item.isInside(rectanglePath.bounds)) return true;
            if (item.intersects(rectanglePath)) return true;
        });
    }

    static lassoSelect(allItems, lassoPath) {
        return allItems.filter((item) => {
            if (item.name === "clipMask") return false;
            if (item.intersects(lassoPath)) return true;
            if (typeof item.image === "object")
                item = new paper.Path.Rectangle({
                    rectangle: item.bounds,
                    insert: false,
                });
            const intersection = lassoPath.intersect(item, {
                insert: false,
            });
            return !!intersection.firstSegment;
        });
    }

    updateCursor(point) {
        const main = $("#main");
        if (typeof point !== "object") {
            main.style.cursor = "default";
            return;
        }
        if (typeof this.#selectedGroup !== "object") return;
        main.style.cursor = this.#selectedGroup?.contains(point)
            ? "move"
            : "default";
        main.style.cursor = MoveItemsTool.getResizeOption(
            point,
            this.#selectedGroup
        ).cursor;
    }

    setStyleOfSelection(newStyle) {
        if (this.#selectedItems.length === 0) return;
        this.#selectedGroup.style = {
            ...this.#selectedGroup.style,
            ...newStyle,
        };
        this.#finishModification();
    }

    #managePageTransfer(pointerPosition) {
        const currentPage = getPageFromPoint(pointerPosition);
        if (currentPage === null || this.page === currentPage) return;
        this.#selectedGroup = currentPage.content.transferSelection(
            this.page.content
        );
        this.page = currentPage;
    }

    #finishModification() {
        const selectedItems = [...this.#selectedItems];
        this.page.content.deselectAll(false);
        this.finishModification();
        this.createCache();
        this.page.content.select(selectedItems);
    }
}
