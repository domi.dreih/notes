import { getFocusedPage } from "../pages/pagemanager.js";
import toolStack from "../tools/toolstack.js";
import { $, $each, hide, show } from "./dom-utils.js";

let currentOverlay = "none";

export function addColorOption(color) {
    const template = $("template");
    const content = template.content.cloneNode(true);
    const input = content.querySelector("input");
    content.querySelector("label").id = `btn-color-${color}`;
    input.value = color;
    content.querySelector("svg").style.color = color;
    $("#toolbarModule-strokeColor").appendChild(content);
    input.addEventListener("input", function () {
        toolStack.setDrawingStyle({ strokeColor: this.value });
    });
}

function openOverlay(overlayID) {
    let nextOverlay = $(`#overlay-${overlayID}`);
    if (nextOverlay) {
        try {
            $(`input[value=${overlayID}]`).checked = true;
        } catch {}
        show(nextOverlay);
        currentOverlay = overlayID;
    }
    if (overlayID === "pageSetup") {
        const page = getFocusedPage();
        const template = page.template;
        $(
            `input[name='radGrp-ruling'][value=${template.ruling}]`
        ).checked = true;
        $("input#switch-paperColor").checked =
            template.backgroundColor === "paper";
    }
}

function closeAllOverlays() {
    try {
        $(`input[value=${currentOverlay}]`).checked = false;
    } catch {}
    $each(".overlay", (overlay) => {
        hide(overlay);
    });
}

export function toggleOverlay(overlayID) {
    closeAllOverlays();
    if (currentOverlay !== overlayID) {
        openOverlay(overlayID);
        return;
    }
    currentOverlay = "none";
}

export function adjustSubmenuPosition() {
    $each(".submenu", (submenu) => {
        const name = submenu.id.split("-")[1];
        const button = document
            .querySelector(`#btn-${name}`)
            .getBoundingClientRect();
        submenu.style.left = `${button.left}px`;
        submenu.style.top = `${button.height}px`;
    });
}

export function setVersionInfo() {
    fetch("/versions")
        .then((body) => body.json())
        .then((body) => {
            const versionInfo = `We are using Paper ${paper.version}, Node ${body.node}, 
            \nV8 ${body.v8} and Electron ${body.electron}`;
            $("#versionInfo").innerText = versionInfo;
        })
        .catch(console.error);
}
