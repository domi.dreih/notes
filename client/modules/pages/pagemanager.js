import { lowerScope, upperScope } from "../docview/papersetup.js";
import { $ } from "../gui/dom-utils.js";
import history from "../history/history.js";
import Modification from "../history/modification.js";
import Page from "./page.js";

let pages = [];
let focusedPage;

$("#main").addEventListener(
    "pointerdown",
    (e) => {
        const pointer = lowerScope.view.getEventPoint(e);
        const page = getPageFromPoint(pointer);
        focusedPage = page !== null ? page : focusedPage;
        highlight(focusedPage);
    },
    true
);

export async function updateAllPages() {
    for (const page of pages) await page.updateAllCaches();
}

export async function addPage(template) {
    const newPageNumber = pages.length;
    const parameters = { number: newPageNumber, top: 0, left: 0, template };
    if (newPageNumber > 0) {
        const lastPage = pages[newPageNumber - 1];
        const margin = 20;
        parameters.top = lastPage.bottom + margin;
        parameters.left = lastPage.left;
        parameters.template ??= lastPage.template;
    }
    await initiatePage(parameters);
}

export async function initiatePage(parameters) {
    const page = new Page(parameters);
    pages.push(page);
    focusedPage = page;
    await page.updateAllCaches();
    $("#main").dispatchEvent(new CustomEvent("page-operation"));
}

export function modifyPageStyle(newStyle) {
    const oldStyle = focusedPage.template;
    let stylesAreEqual = true;
    for (const key in newStyle) {
        const oldValue = oldStyle[key];
        const newValue = newStyle[key];
        stylesAreEqual &&= oldValue === newValue;
    }
    if (stylesAreEqual) return;
    focusedPage.template = newStyle;
    const undo = () => {
        focusedPage.template = oldStyle;
    };
    const redo = () => {
        focusedPage.template = newStyle;
    };
    history.add(new Modification({ undo, redo }));
}

function highlight(page) {
    const shadowBox =
        upperScope.project.activeLayer.children.find(
            (item) => item.name === "shadowBox"
        ) ??
        new paper.Path.Rectangle({
            rectangle: page,
            strokeColor: "rgba(255,255,255,1)",
            fillColor: "rgba(0,0,0,0)",
            shadowColor: "rgb(0,0,0)",
            shadowBlur: 10,
            name: "shadowBox",
        });
    shadowBox.bounds = page;
    shadowBox.sendToBack();
}

export function getPageFromPoint(point) {
    for (const page of pages) {
        if (page.contains(point)) return page;
    }
    return null;
}

export function getPageFromIndex(i) {
    return pages[i];
}

export function getFocusedPage() {
    return focusedPage;
}

export function getDocumentBounds() {
    if (pages.length < 1) return new paper.Rectangle(0, 0, 0, 0);
    const firstPage = pages[0];
    const lastPage = pages[pages.length - 1];
    return new paper.Rectangle(firstPage.topLeft, lastPage.bottomRight);
}

export function getContentCache() {
    return pages.map((page) => page.content.exportJSON());
}

export function serializePages(options) {
    return pages.map((page) => page.serialize(options));
}

export function clearPages() {
    pages.forEach((page) => page.content.deselectAll());
    pages = [];
}
