import { $, hide, show } from "../gui/dom-utils.js";
import { getDocumentBounds } from "../pages/pagemanager.js";
import { lowerScope, upperScope } from "./papersetup.js";

const BUTTON_THRESHOLD = 100;

export function scroll({ x = 0, y = 0 }) {
    const dif = new paper.Point(x, y);
    upperScope.activate();
    upperScope.view.translate(dif);
    lowerScope.view.translate(dif);

    const totalBounds = rectangleToView(getDocumentBounds());
    const button = $("#btn-addPage");
    const bottomOfPages = totalBounds.bottomCenter.y;
    const { viewSize } = lowerScope.view;
    if (bottomOfPages < viewSize.height - BUTTON_THRESHOLD) show(button);
    else hide(button);
}

export function scale(factor) {
    lowerScope.view.scale(factor);
    upperScope.view.scale(factor);
}

export function rectangleToView(rectangle) {
    const { topLeft, bottomRight } = rectangle;
    const transformedTopLeft = upperScope.view.projectToView(topLeft);
    const transformedBottomRight = upperScope.view.projectToView(bottomRight);
    return new paper.Rectangle(transformedTopLeft, transformedBottomRight);
}
