import { upperScope } from "../../docview/papersetup.js";
import { $ } from "../../gui/dom-utils.js";
import { getFocusedPage } from "../../pages/pagemanager.js";

let pointerTypeIsValid = false;
$("#main").addEventListener("pointerdown", (e) => {
    pointerTypeIsValid = e.pointerType !== "touch";
});

export default class EditingTool extends paper.Tool {
    name;
    type;
    page;
    #pointerIsValid = false;

    constructor(name, type, handlers) {
        upperScope.activate();
        super();
        this.name = name;
        this.type = type;

        for (const handlerType in handlers) {
            if (!Object.hasOwnProperty.call(handlers, handlerType)) continue;
            const handler = handlers[handlerType];
            const needsPointerValidation = [
                "onMouseDrag",
                "onMouseUp",
            ].includes(handlerType);
            this[handlerType] = function (event) {
                if (needsPointerValidation && !this.#pointerIsValid) return;
                handler(event);
            };
        }

        this.onMouseDown = (event) => {
            const focusedPage = getFocusedPage();
            this.#pointerIsValid =
                pointerTypeIsValid &&
                focusedPage !== null &&
                (event.event.button === 0 || event.event.which === 0);
            if (!this.#pointerIsValid) return;
            this.page = focusedPage;
            handlers.onMouseDown(event);
        };
    }
}
