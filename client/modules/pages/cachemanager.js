import { lowerScope, upperScope } from "../docview/papersetup.js";

export default class CacheManager {
    #modelLayer = new paper.Layer();
    #cacheGroup = new paper.Group();
    #currentRaster = new paper.Raster();
    #rasterSupplier;
    #updateObservers = [];
    #currentZoom;
    constructor({ name, modelItems, displayAboveCache, rasterSupplier }) {
        lowerScope.project.insertLayer(0, this.#modelLayer);
        upperScope.project.activeLayer.addChild(this.#cacheGroup);
        this.#modelLayer.name = `${name}-modelLayer`;
        this.#modelLayer.addChildren(modelItems);

        this.#cacheGroup.name = `${name}-cacheGroup`;
        this.#currentRaster.name = `${name}-currentRaster`;
        this.#cacheGroup.addChild(this.#currentRaster);
        this.#cacheGroup.addChildren(displayAboveCache);

        if (typeof rasterSupplier !== "function")
            rasterSupplier = this.#defaultRasterizer;
        this.#rasterSupplier = rasterSupplier;
    }
    #defaultRasterizer() {
        return this.#modelLayer.rasterize({
            resolution: upperScope.view.resolution * this.#currentZoom,
            insert: false,
        });
    }
    async updateCache(isDirty = false) {
        if (!isDirty && this.#currentZoom === upperScope.view.zoom) return;
        this.#currentZoom = upperScope.view.zoom;
        const newRaster = this.#rasterSupplier();
        if (newRaster instanceof Promise)
            this.#setCurrentRaster(await newRaster);
        else this.#setCurrentRaster(newRaster);
    }
    #setCurrentRaster(newRaster) {
        if (!(newRaster instanceof paper.Raster)) return;
        newRaster.name = this.#currentRaster.name;
        this.#currentRaster.replaceWith(newRaster);
        this.#currentRaster = newRaster;
        for (const observer of this.#updateObservers) {
            observer();
        }
    }
    addUpdateObserver(observer) {
        if (typeof observer !== "function") return;
        this.#updateObservers.push(observer);
    }
}
