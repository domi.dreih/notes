import { cm_to_pt } from "../mathutils.js";
import { renderPDFPage } from "../pdf-rendering.js";
import CacheManager from "./cachemanager.js";

export default class PageBackground extends CacheManager {
    static #paperColor = "#f7f6e8";
    #page;
    #backgroundColor;
    #backgroundBox;
    #backgroundGroup;
    #ruling;
    #rulingColor;
    #rulingParameters;
    #rulingSymbols;
    #rulingGroup;
    #pdfBackground;
    constructor(
        page,
        {
            backgroundColor,
            ruling,
            rulingColor,
            rulingParameters,
            pdfBackground,
        }
    ) {
        const pageNumber = page.number;
        const backgroundGroup = new paper.Group({
            name: `backgroundGroup${pageNumber}`,
        });
        const backgroundBox = new paper.Path.Rectangle({
            rectangle: page,
            name: `backgroundBox${pageNumber}`,
        });
        const rulingGroup = new paper.Group({
            name: `rulingGroup${pageNumber}`,
        });
        backgroundGroup.addChildren([backgroundBox, rulingGroup]);
        const hasPDFPage = PageBackground.#isPDFBackgroundValid(pdfBackground);
        const rasterSupplier = hasPDFPage
            ? () => this.#renderPDFBackground()
            : undefined;
        super({
            name: `background${pageNumber}`,
            modelItems: [backgroundGroup],
            rasterSupplier,
        });
        this.#backgroundGroup = backgroundGroup;
        this.#backgroundBox = backgroundBox;
        this.#rulingGroup = rulingGroup;

        this.#page = page;
        this.#setBackgroundColor(backgroundColor ?? "white");
        this.#setRulingColor(rulingColor ?? "#808080");

        const horizontalLine = new paper.Path.Line({
            from: page.topLeft,
            to: page.topRight,
            insert: false,
            ...this.#rulingStyle,
        });
        const verticalLine = new paper.Path.Line({
            from: page.topLeft,
            to: page.bottomLeft,
            insert: false,
            ...this.#rulingStyle,
        });
        const dot = new paper.Path.Circle({
            center: page.topLeft,
            radius: 0.2,
            insert: false,
            ...this.#rulingStyle,
        });
        this.#rulingSymbols = { horizontalLine, verticalLine, dot };

        this.#pdfBackground = pdfBackground ?? null;
        this.#setRuling(ruling ?? "noLines", rulingParameters);
    }

    static #getDefaultRulingParameters(ruling) {
        switch (ruling) {
            case "lined": {
                const spacing = cm_to_pt(0.9);
                return { spacing, offset: spacing };
            }
            case "squared":
            case "dotted": {
                const spacing = cm_to_pt(0.5);
                return { spacing, offset: spacing / 2 };
            }
            case "noLines":
            default:
                return {};
        }
    }

    static #isPDFBackgroundValid(pdfBackground) {
        if (!pdfBackground) return false;
        if (typeof pdfBackground.pageNumber !== "number") return false;
        return typeof pdfBackground.fingerprint === "string";
    }
    #renderPDFBackground() {
        return new Promise(async (resolve) => {
            try {
                const raster = await renderPDFPage(
                    this.#pdfBackground,
                    this.#page
                );
                raster.onLoad = () => {
                    raster.fitBounds(this.#page);
                    resolve(raster);
                };
            } catch (error) {
                resolve();
            }
        });
    }
    get #rulingStyle() {
        return {
            strokeColor: this.#rulingColor,
            fillColor: this.#rulingColor,
            strokeScaling: false,
        };
    }
    #addRulingItem(item) {
        this.#rulingGroup.addChild(item);
    }
    #addVerticalLine(x) {
        const instance = this.#rulingSymbols.verticalLine.clone();
        instance.position.x = x + this.#page.left;
        this.#addRulingItem(instance);
    }
    #addHorizontalLine(y) {
        const instance = this.#rulingSymbols.horizontalLine.clone();
        instance.position.y = y + this.#page.top;
        this.#addRulingItem(instance);
    }
    #addNewDot(position) {
        const instance = this.#rulingSymbols.dot.clone();
        instance.position = position.add(this.#page.topLeft);
        this.#addRulingItem(instance);
    }
    #setBackgroundColor(newColor) {
        if (typeof newColor !== "string") return false;
        if (this.#backgroundColor === newColor) return false;
        this.#backgroundColor = newColor;
        if (newColor === "paper") newColor = PageBackground.#paperColor;
        this.#backgroundBox.fillColor = newColor;
        return true;
    }
    #setRuling(newRuling, parameters) {
        if (typeof newRuling !== "string") return false;
        if (this.#ruling === newRuling) return false;
        if (!newRuling.match(/noLines|lined|squared|dotted/)) return false;
        if (typeof parameters !== "object")
            parameters = PageBackground.#getDefaultRulingParameters(newRuling);
        this.#ruling = newRuling;
        this.#rulingParameters = parameters;
        this.#rulingGroup.removeChildren();
        if (newRuling === "noLines") return true;

        const { spacing, offset } = parameters;
        const columnCount = (this.#page.width - offset) / spacing;
        const rowCount = (this.#page.height - offset) / spacing;
        switch (newRuling) {
            case "lined": {
                for (let y = 0; y < rowCount; y++) {
                    this.#addHorizontalLine(y * spacing + offset);
                }
                break;
            }
            case "squared": {
                for (let x = 0; x < columnCount; x++) {
                    this.#addVerticalLine(x * spacing + offset);
                }
                for (let y = 0; y < rowCount; y++) {
                    this.#addHorizontalLine(y * spacing + offset);
                }
                break;
            }
            case "dotted": {
                const offsetVector = new paper.Point(offset, offset);
                for (let x = 0; x < columnCount; x++) {
                    for (let y = 0; y < rowCount; y++) {
                        const point = new paper.Point(x, y)
                            .multiply(spacing)
                            .add(offsetVector);
                        this.#addNewDot(point);
                    }
                }
                break;
            }
            default:
                break;
        }
        return true;
    }
    #setRulingColor(newColor) {
        if (typeof newColor !== "string") return false;
        if (this.#rulingColor === newColor) return false;
        this.#rulingColor = newColor;
        this.#rulingGroup.set({ strokeColor: newColor, fillColor: newColor });
        return true;
    }
    get descriptor() {
        return {
            backgroundColor: this.#backgroundColor,
            ruling: this.#ruling,
            rulingColor: this.#rulingColor,
            rulingParameters: this.#rulingParameters,
            pdfBackground: this.#pdfBackground,
        };
    }
    set({ backgroundColor, ruling, rulingColor }) {
        let backgroundChanged = false;
        backgroundChanged ||= this.#setBackgroundColor(backgroundColor);
        backgroundChanged ||= this.#setRuling(ruling);
        backgroundChanged ||= this.#setRulingColor(rulingColor);
        if (backgroundChanged) this.updateCache(true);
    }

    exportJSON() {
        return this.#backgroundGroup.exportJSON();
    }
    exportSVG() {
        const { left, top } = this.#backgroundGroup.bounds;
        this.#backgroundGroup.translate({ x: -left, y: -top });
        const svg = this.#backgroundGroup.exportSVG({
            embedImages: false,
            matchShapes: true,
        });
        this.#backgroundGroup.translate({ x: left, y: top });
        return svg.outerHTML;
    }
}
