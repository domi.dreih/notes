import { snapToGrid } from "../../mathutils.js";
import DrawingTool from "./drawingtool.js";

export default class GeometryTool extends DrawingTool {
    #origin;
    static snapsToGrid = false;

    constructor(name, { createShape, updateShape }) {
        const onMouseDown = (event) => {
            this.#origin = GeometryTool.snapsToGrid
                ? snapToGrid(event.point)
                : event.point;
            createShape(this.#origin);
        };

        const onMouseDrag = (event) => {
            const point = GeometryTool.snapsToGrid
                ? snapToGrid(event.point)
                : event.point;
            updateShape(point, this.#origin);
        };

        const onMouseUp = () => {
            this.finish(this.shape);
        };

        const handlers = {
            onMouseDown,
            onMouseDrag,
            onMouseUp,
        };
        super(name, "geometry", handlers);
    }
}
