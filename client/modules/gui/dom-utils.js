export function $(selector) {
    return document.querySelector(selector);
}

export function $each(selector, callback) {
    document.querySelectorAll(selector).forEach(callback);
}

export function show(element) {
    element.classList.remove("hidden");
}

export function hide(element) {
    element.classList.add("hidden");
}
