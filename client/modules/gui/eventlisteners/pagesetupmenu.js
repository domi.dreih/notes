import { modifyPageStyle } from "../../pages/pagemanager.js";
import { $, $each } from "../dom-utils.js";
import { toggleOverlay } from "../gui-actions.js";

$("#main").addEventListener("mousedown", (e) => {
    // open page setup menu when user right-clicks on page:
    if (e.button === 2) {
        toggleOverlay("pageSetup");
    }
});

$each("input[name='radGrp-ruling']", (input) => {
    const ruling = input.value;
    input.addEventListener("input", () => {
        modifyPageStyle({ ruling });
    });
});

$("input#switch-paperColor").addEventListener("input", function () {
    modifyPageStyle({ backgroundColor: this.checked ? "paper" : "white" });
});
