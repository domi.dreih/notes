import { upperScope } from "./docview/papersetup.js";

const pdfjsLib = window["pdfjs-dist/build/pdf"];
pdfjsLib.GlobalWorkerOptions.workerSrc = "/pdf/pdf.worker.js";

let pdfDocuments = {};
let data = {};

export async function addPDFData(base64Data) {
    const pdf = await decodePDFDocument(base64Data);
    const fingerprint = pdf.fingerprints[0];
    data[fingerprint] = base64Data;
    return { pdf, fingerprint };
}

async function decodePDFDocument(base64Data) {
    if (typeof base64Data !== "string") return;
    const loadingTask = pdfjsLib.getDocument(base64Data);
    const pdf = await loadingTask.promise;
    const fingerprint = pdf.fingerprints[0];
    pdfDocuments[fingerprint] = pdf;
    return pdf;
}

export async function renderPDFPage({ fingerprint, pageNumber }, rectangle) {
    if (!pageNumber)
        throw new Error(
            "Invalid Argument: pageNumber has to be a positive Integer."
        );
    if (typeof pdfDocuments[fingerprint] !== "object") return;

    const page = await pdfDocuments[fingerprint].getPage(pageNumber);
    let viewport = page.getViewport({ scale: 1 });
    const scaledPageWidth = upperScope.view.projectToView(
        new paper.Point(rectangle.size)
    ).x;
    const scale = scaledPageWidth / viewport.width;
    viewport = page.getViewport({ scale });

    const canvas = document.querySelector("#pdf-canvas");
    const canvasContext = canvas.getContext("2d");
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    const renderContext = { canvasContext, viewport };
    const renderTask = page.render(renderContext);
    await renderTask.promise;

    const source = canvas.toDataURL();
    return new paper.Raster({
        source,
        position: rectangle.center,
        insert: false,
    });
}

export async function setPDFData(pdfData) {
    data = pdfData;
    for (const fingerprint in pdfData) {
        const base64Data = pdfData[fingerprint];
        await decodePDFDocument(base64Data);
    }
}

export function getPDFData() {
    return data;
}

export function clearPDFData() {
    pdfDocuments = {};
    data = {};
}
