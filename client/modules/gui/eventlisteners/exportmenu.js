import { fileEventHandlers } from "../../file.js";
import { $ } from "../dom-utils.js";
import { toggleOverlay } from "../gui-actions.js";

$("button[value='cancelExport']").addEventListener("click", () => {
    toggleOverlay("export");
});

$("button[value='export']").addEventListener("click", () => {
    toggleOverlay("export");
    const options = {
        includeRuling: $("#switch-includeRuling").checked,
        includePDFBackground: $("#switch-includePDFBackground").checked,
    };
    fileEventHandlers.exportPDF(options);
});
