import { lowerScope } from "../docview/papersetup.js";
import history from "../history/history.js";
import { getFocusedPage } from "../pages/pagemanager.js";

export async function insertImage() {
    const res = await fetch("/insert/image");
    if (!res.ok) return;
    switch (res.headers.get("content-type")) {
        case "image/svg+xml":
            const svg = await res.text();
            const group = new paper.Group();
            group.importSVG(svg);
            processImage(group);
            break;
        case "image/jpeg":
        case "image/png":
        case "image/bmp":
            const blob = await res.blob();
            const reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onload = () => {
                const raster = new paper.Raster({
                    source: reader.result,
                    visible: false,
                });
                raster.onLoad = () => {
                    processImage(raster);
                };
            };
            break;
        default:
            break;
    }
}

function processImage(image) {
    const page = getFocusedPage();
    const bounds = lowerScope.view.bounds.intersect(page);
    image.position = bounds.center;
    image.fitBounds(bounds.scale(0.8));
    image.visible = true;
    const modification = page.content.add(image);
    history.add(modification);
}
