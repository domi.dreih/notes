import GeometryTool from "../abstracttools/geometrytool.js";

export default class RectangleTool extends GeometryTool {
    constructor() {
        const createRect = (point) => {
            this.shape = new paper.Path.Rectangle(point, point);
        };

        const updateRect = (point, origin) => {
            this.shape.remove();
            this.shape = new paper.Path.Rectangle(origin, point);
        };

        const handlers = {
            createShape: createRect,
            updateShape: updateRect,
        };
        super("rectangle", handlers);
    }
}
