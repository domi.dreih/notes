import { $ } from "../gui/dom-utils.js";

export const lowerScope = new paper.PaperScope();
lowerScope.setup(new OffscreenCanvas(500, 500));
lowerScope.project.view.autoUpdate = false;

export const upperScope = new paper.PaperScope();
upperScope.setup("view");

const cacheLayer = new paper.Layer();
cacheLayer.addTo(upperScope.project);

lowerScope.activate();

const main = $("#main");
resizeScopes(main.offsetWidth, main.offsetHeight);

export function resizeScopes(width, height) {
    const newSize = new paper.Size(width, height);
    lowerScope.project.view.viewSize = newSize;
    upperScope.project.view.viewSize = newSize;
}
