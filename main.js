const Fs = require("fs");
const Zlib = require("zlib");
const { app, BrowserWindow, dialog } = require("electron");
const Express = require("express");
const { exportPDF } = require("./server/pdf-export.js");
const expressApp = Express();
const httpServer = require("http").createServer(expressApp);
const io = require("socket.io")(httpServer, { maxHttpBufferSize: 1e10 });
let win;

if (require("electron-squirrel-startup")) app.quit();
const isDebugging = process.argv.includes("--debug");

// Only serve to localhost
expressApp.use(function (req, res, next) {
    if (req.ip.endsWith("127.0.0.1") || req.ip.endsWith("::1")) next();
    else res.sendStatus(401).end();
});

expressApp.use("/", Express.static(`${__dirname}/client`));
expressApp.use(Express.json({ limit: "100mb" }));
expressApp.use(
    "/paper",
    Express.static(`${__dirname}/node_modules/paper/dist`)
);
expressApp.use(
    "/pdf",
    Express.static(`${__dirname}/node_modules/pdfjs-dist/build`)
);

expressApp.get("/versions", (req, res) => {
    res.send(process.versions);
});

io.on("connection", (socket) => {
    function getClientState() {
        return new Promise((resolve, reject) => {
            socket.emit("getCurrentState", async (clientState) => {
                resolve(clientState);
            });
        });
    }

    function setClientState(newClientState) {
        socket.emit("setCurrentState", newClientState);
    }

    async function confirmAllSaved() {
        function displayPrompt() {
            const selectedOption = dialog.showMessageBoxSync(win, {
                type: "question",
                buttons: ["Speichern", "Nicht Speichern", "Abbrechen"],
                defaultId: 0,
                title: "TwoNotes: Nicht gespeicherte Änderungen",
                message:
                    "Es gibt nicht gespeicherte Änderungen in dieser Datei, die beim Fortfahren ohne Speichern verloren gehen. Möchten Sie diese speichern?",
                cancelId: 2,
            });
            return selectedOption;
        }
        async function userWantsToContinue() {
            const allChangesSaved = (await getClientState()).allChangesSaved;
            if (allChangesSaved) return true;
            const selectedOption = displayPrompt();
            if (selectedOption === 0) return await saveListener();
            return selectedOption === 1;
        }
        return await userWantsToContinue();
    }

    async function saveAsDialog() {
        const path = dialog.showSaveDialogSync(win, {
            title: "Speichern unter...",
            properties: ["showOverwriteConfirmation"],
            filters: [
                {
                    name: "TwoNotes Datei",
                    extensions: ["tnts"],
                },
                {
                    name: "Alle Dateien",
                    extensions: ["*"],
                },
            ],
        });
        if (path === undefined || path.length === 0) return false;
        if (!path.endsWith(".tnts")) path += ".tnts";
        try {
            setClientState({ filePath: path });
            await save(path);
            return true;
        } catch (e) {
            console.error(
                `An error ocurred processing the file "${path}":\n${e}`
            );
            return false;
        }
    }

    function save(path) {
        return new Promise((resolve, reject) => {
            socket.emit("getSerializedContent", (content) => {
                try {
                    const json = Zlib.gzipSync(JSON.stringify(content));
                    Fs.writeFileSync(path, json);
                    setClientState({ allChangesSaved: true });
                    resolve();
                } catch (error) {
                    reject(error);
                }
            });
        });
    }

    async function openDialog() {
        const continues = await confirmAllSaved();
        if (!continues) return false;
        const result = dialog.showOpenDialogSync(win, {
            title: "Datei öffnen",
            properties: ["openFile"],
            filters: [
                {
                    name: "TwoNotes Dateien",
                    extensions: ["tnts"],
                },
                {
                    name: "Alle Dateien",
                    extensions: ["*"],
                },
            ],
        });
        if (typeof result !== "object") return false;
        const path = result[0];
        if (!Fs.existsSync(path)) return false;
        let json = JSON.parse(Zlib.gunzipSync(Fs.readFileSync(path)));
        json.path = path;
        return json;
    }

    async function saveListener() {
        const filePath = (await getClientState()).filePath;
        if (filePath === "") return await saveAsDialog();
        await save(filePath);
        return true;
    }

    socket.on("confirmAllSaved", (callback) => {
        confirmAllSaved().then(callback, () => callback(false));
    });
    socket.on("saveAsDialog", saveAsDialog);
    socket.on("save", saveListener);
    socket.on("openDialog", (callback) => {
        openDialog().then(callback, () => callback(false));
    });
    socket.on("exportPDF", (json, options) => exportPDF(json, options, win));
    win.on("close", (e) => {
        e.preventDefault();
        confirmAllSaved()
            .then((continues) => {
                if (continues) win.destroy();
            })
            .catch(() => {});
    });
});

expressApp.get("/insert/image", (req, res) => {
    const result = dialog.showOpenDialogSync(win, {
        title: "Bild einfügen",
        properties: ["openFile"],
        filters: [
            {
                name: "Bilddateien",
                extensions: ["jpg", "jpeg", "png", "bmp", "svg"],
            },
            {
                name: "Alle Dateien",
                extensions: ["*"],
            },
        ],
    });
    if (result === undefined) {
        res.sendStatus(404).end();
        return;
    }
    const path = result[0];
    if (!Fs.existsSync(path)) {
        res.sendStatus(404).end();
        return;
    }
    res.sendFile(path);
});

expressApp.get("/insert/pdf", (req, res) => {
    const result = dialog.showOpenDialogSync(win, {
        title: "PDF-Datei einfügen",
        properties: ["openFile"],
        filters: [
            {
                name: "PDF-Dateien",
                extensions: ["pdf"],
            },
            {
                name: "Alle Dateien",
                extensions: ["*"],
            },
        ],
    });
    if (result === undefined) {
        res.sendStatus(404).end();
        return;
    }
    const path = result[0];
    if (!Fs.existsSync(path)) {
        res.sendStatus(404).end();
        return;
    }
    res.sendFile(path);
});

const listener = httpServer.listen(2480);

function createWindow() {
    // Erstelle das Browser-Fenster.
    win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
        },
    });
    win.maximize();

    // und lade die index.html der app.
    win.loadURL("http://localhost:2480/index.html");
    if (isDebugging) {
        win.webContents.openDevTools();
    } else {
        win.setMenu(null);
    }
}

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
    app.quit();
});

app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

// Properly exit app
for (const signal of ["SIGINT", "SIGTERM", "SIGHUP"])
    process.on(signal, () => {
        console.log(app.quit());
        listener.close(() => {
            process.exit(0);
        });
    });
