import { finishNewFilePreparation } from "./file.js";
import { addPage } from "./pages/pagemanager.js";
import { addPDFData } from "./pdf-rendering.js";

export async function importPDF() {
    await appendPDF();
    finishNewFilePreparation();
}

export function appendPDF() {
    return new Promise(async (resolve, reject) => {
        const res = await fetch("/insert/pdf");
        if (!res.ok) reject();
        if (res.headers.get("content-type") !== "application/pdf") reject();
        const blob = await res.blob();
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onload = async () => {
            const base64Data = reader.result;
            const { pdf, fingerprint } = await addPDFData(base64Data);

            // Beware: PDF page numbers start with 1, not 0
            for (let i = 1; i <= pdf.numPages; i++) {
                const pdfBackground = { pageNumber: i, fingerprint };
                const page = await pdf.getPage(i);
                const { width, height } = page.getViewport({ scale: 1 });
                await addPage({ width, height, pdfBackground });
            }
            resolve();
        };
    });
}
