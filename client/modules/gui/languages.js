import { $each } from "./dom-utils.js";
import { adjustSubmenuPosition } from "./gui-actions.js";

let currentLang;
let currentLangFile = "de_DE";

class LanguageText extends HTMLElement {
    constructor() {
        super();
        this.addEventListener("update-lang", () => {
            this.innerText =
                currentLang?.[this.dataset?.key] ?? this.dataset?.key ?? "???";
        });
    }
}

export function initLang() {
    customElements.define("lang-txt", LanguageText);
    changeLang(currentLangFile);
}

function updateLang() {
    $each("lang-txt", (element) =>
        element.dispatchEvent(new CustomEvent("update-lang"))
    );
    adjustSubmenuPosition();
}

export function changeLang(langToFetch) {
    fetch(`lang/${langToFetch}.json`)
        .then((body) => body.json())
        .then((langData) => {
            currentLang = langData;
            updateLang();
        })
        .catch(console.error);
}
