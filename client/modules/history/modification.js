export default class Modification {
    undo = () => {};
    redo = () => {};
    constructor({ undo, redo }) {
        this.undo = undo;
        this.redo = redo;
    }
}
