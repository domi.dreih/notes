import Modification from "../history/modification.js";
import CacheManager from "./cachemanager.js";

export default class PageContent extends CacheManager {
    #group;
    #precache;
    #selection;
    #selectionOutline = new paper.Path.Rectangle();
    constructor(page, contentJSON) {
        const pageNumber = page.number;
        const group = new paper.Group({ name: `contentGroup${pageNumber}` });
        const precache = new paper.Group({ name: `precache${pageNumber}` });
        const selection = new paper.Group({ name: `selection${pageNumber}` });
        const contentClipMask = new paper.Path.Rectangle({
            rectangle: page,
            clipMask: true,
        });
        const precacheClipMask = contentClipMask.clone({ insert: false });
        super({
            name: `content${pageNumber}`,
            modelItems: [group],
            displayAboveCache: [precache, selection],
        });
        this.#group = group;
        this.#group.addChild(contentClipMask);
        contentClipMask.name = "clipMask";
        this.#precache = precache;
        this.#precache.addChild(precacheClipMask);
        precacheClipMask.name = "clipMask";
        this.#selection = selection;
        this.#selection.addChild(this.#selectionOutline);

        this.addUpdateObserver(() => {
            this.clearPrecache(false);
        });
        if (typeof contentJSON === "undefined") return;
        this.deserialize(contentJSON, false);
    }
    add(item, updateCache = true) {
        this.#group.addChild(item);
        if (updateCache) this.updateCache(true);
        return this.#createAddingModification(item);
    }
    remove(item, updateCache = true) {
        item.remove();
        if (updateCache) this.updateCache(true);
    }
    addToPrecache(item) {
        this.#precache.addChild(item);
    }
    clearPrecache(updateCache = true) {
        const clipMask = this.#precache.children.clipMask;
        this.#precache.removeChildren();
        this.#precache.addChild(clipMask);
        if (updateCache) this.updateCache(true);
    }
    select(items, updateCache = true) {
        if (items.length === 0) {
            return;
        }
        this.#selection.addChildren(items);
        this.#selection.selected = false;
        this.#selectionOutline.remove();
        const newOutline = new paper.Path.Rectangle(this.#selection.bounds);
        this.#selection.addChild(newOutline);
        this.#selectionOutline = newOutline;
        this.#selectionOutline.name = "selectionOutline";
        this.#selection.selected = true;
        if (updateCache) this.updateCache(true);
        return this.#selection;
    }
    deselectAll(updateCache = true) {
        this.#selection.selected = false;
        this.#selectionOutline.remove();
        this.#group.addChildren(this.#selection.removeChildren());
        if (updateCache) this.updateCache(true);
    }
    transferSelection(source) {
        const ownOutline = this.#selectionOutline;
        ownOutline.remove();
        const temp = source.#selection.removeChildren();
        this.#selection.addChildren(temp);
        this.#selectionOutline = source.#selectionOutline;
        source.#selectionOutline = ownOutline;
        return this.#selection;
    }
    get group() {
        return this.#group;
    }
    exportJSON() {
        this.deselectAll();
        return this.#group.exportJSON();
    }
    exportSVG() {
        this.deselectAll();
        const { left, top } = this.#group.bounds;
        this.#group.translate({ x: -left, y: -top });
        const svg = this.#group.exportSVG({
            embedImages: false,
            matchShapes: true,
        });
        this.#group.translate({ x: left, y: top });
        return svg.outerHTML;
    }

    deserialize(json, updateCache = true) {
        this.deselectAll();
        const tempGroup = new paper.Group().importJSON(json);
        this.#group.removeChildren();
        this.#group.addChildren(tempGroup.removeChildren());
        tempGroup.remove();
        if (updateCache) this.updateCache(true);
    }
    #createAddingModification(addedItem) {
        const addedJSON = addedItem.exportJSON();
        const { index, className } = addedItem;
        const undo = () => {
            this.deselectAll();
            this.#group.children[index].remove();
            this.updateCache(true);
        };
        const redo = () => {
            this.deselectAll();
            const item = new paper[className]();
            item.importJSON(addedJSON);
            this.#group.addChild(item);
            this.updateCache(true);
        };
        return new Modification({ undo, redo });
    }
}
