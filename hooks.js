const sass = require("sass");
const { writeFile } = require("fs/promises");

const outPath = "client/styles/style.css";
async function generateAssets() {
    const result = await sass.compileAsync("client/styles-src/style.scss", {
        style: "compressed",
        sourceMap: true,
    });
    await writeFile(
        outPath,
        `${result.css}/*# sourceMappingURL=style.css.map */`
    );
    await writeFile(`${outPath}.map`, JSON.stringify(result.sourceMap));
}

module.exports = { generateAssets };
