import history from "../../history/history.js";
import EditingTool from "./editingtool.js";

export default class DrawingTool extends EditingTool {
    static style = {
        strokeColor: "#000000",
        strokeWidth: 1,
        strokeCap: "round",
        strokeJoin: "round",
    };
    shape;

    constructor(name, type, _handlers) {
        const onMouseDown = (event) => {
            _handlers.onMouseDown(event);
            this.shape.style = DrawingTool.style;
            this.page.content.addToPrecache(this.shape);
        };

        const onMouseDrag = (event) => {
            _handlers.onMouseDrag(event);
            this.shape.style = DrawingTool.style;
            this.page.content.addToPrecache(this.shape);
        };

        const handlers = { ..._handlers, onMouseDown, onMouseDrag };
        super(name, type, handlers);
    }

    finish(shape, updateCache = true) {
        const modification = this.page.content.add(shape, updateCache);
        history.add(modification);
    }
}
