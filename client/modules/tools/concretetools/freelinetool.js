import DrawingTool from "../abstracttools/drawingtool.js";

export default class FreeLineTool extends DrawingTool {
    #pointerWasWithinPage;
    #shapeIsFinished;

    constructor() {
        const onMouseDown = (event) => {
            this.#shapeIsFinished = false;
            this.shape = new paper.Path();
            this.shape.add(event.point);
        };

        const onMouseDrag = (event) => {
            const { point } = event;
            const newPointerWithinPage = this.page.contains(point);
            if (this.#pointerWasWithinPage && newPointerWithinPage)
                return this.shape.add(event.point);
            if (!this.#pointerWasWithinPage && newPointerWithinPage)
                onMouseDown(event);
            if (this.#pointerWasWithinPage && !newPointerWithinPage)
                onMouseUp();
            this.#pointerWasWithinPage = newPointerWithinPage;
        };

        const onMouseUp = () => {
            if (this.#shapeIsFinished) return;
            this.shape.simplify(0.5);
            this.finish(this.shape.clone(), false);
            this.#shapeIsFinished = true;
        };

        const onDeactivate = () => {
            this.page?.content?.clearPrecache();
        };

        const handlers = {
            onMouseDown,
            onMouseDrag,
            onMouseUp,
            onDeactivate,
        };
        super("freeLine", "draw", handlers);
    }
}
