const marginSize = 50;
const marginResistance = 3;
const marginFriction = 1.5;
const innerFriction = 1.025;
const velocityCutoff = 0.2;

export default class ScrollDimension {
    #lowerSoftLimit = 0;
    #upperSoftLimit = 0;
    #lowerHardLimit = 0;
    #upperHardLimit = 0;
    #viewSize = 0;
    #pageSize = 0;
    #currentPosition = 0;
    #currentVelocity = 0;
    #isAnimating = false;
    #animatingSpringback = false;
    #scrollOutput;

    constructor(pageSize = 0, viewSize = 0, scrollOutput = () => {}) {
        this.#pageSize = pageSize;
        this.#viewSize = viewSize;
        this.#scrollOutput = scrollOutput;
        this.#updateLimits(viewSize, pageSize);
    }

    #updateLimits(viewSize, pageSize) {
        if (typeof viewSize !== "number" || typeof pageSize !== "number")
            return;
        if (viewSize === 0 || pageSize === 0) return;
        const setLimits = (factor) => {
            this.#upperSoftLimit = (this.#pageSize * factor) / 2;
            this.#lowerSoftLimit = -this.#upperSoftLimit;
            this.#upperHardLimit = this.#upperSoftLimit + marginSize;
            this.#lowerHardLimit = -this.#upperHardLimit;
        };
        const sizeRatio = pageSize / viewSize;
        const lowerThreshold = 0.7;
        const upperThreshold = 1.0;
        if (sizeRatio <= lowerThreshold) {
            setLimits(0);
            return;
        }
        if (sizeRatio >= upperThreshold) {
            setLimits(1);
            return;
        }
        setLimits(
            (1 / (upperThreshold - lowerThreshold)) * sizeRatio -
                lowerThreshold / (upperThreshold - lowerThreshold)
        );
    }

    /**
     * @param {number} viewSize
     */
    setViewSize(viewSize, updatePosition = false) {
        if (viewSize === this.#viewSize) return;
        if (updatePosition && this.#viewSize !== 0)
            this.#currentPosition -= (viewSize - this.#viewSize) / 2;
        this.#viewSize = viewSize;
        this.#updateLimits(this.#viewSize, this.#pageSize);
    }

    /**
     * @param {number} pageSize
     */
    set pageSize(pageSize) {
        if (pageSize === this.#pageSize) return;
        this.#currentPosition += (pageSize - this.#pageSize) / 2;
        this.#pageSize = pageSize;
        this.#updateLimits(this.#viewSize, this.#pageSize);
    }

    #scrollConstrained(delta) {
        const oldPosition = this.#currentPosition;
        this.#currentPosition = Math.max(
            this.#lowerHardLimit,
            Math.min(this.#upperHardLimit, oldPosition + delta)
        );
        this.#scrollOutput(this.#currentPosition - oldPosition);
    }

    handleScrollInput(delta) {
        this.stopAnimation();
        if (this.#upperSoftLimit === 0) return;
        if (this.#isWithinSoftLimits) this.#scrollConstrained(delta);
        else this.#scrollConstrained(delta / marginResistance);
    }
    /**
     * @param {number} inertia
     */
    set inertia(inertia) {
        if (this.#upperSoftLimit === 0) return;
        this.#currentVelocity = inertia;
        this.#isAnimating = true;
    }

    startAnimation() {
        if (!this.#isAnimating && !this.#isWithinSoftLimits) {
            this.#startSpringback();
        }
    }

    stopAnimation() {
        this.#currentVelocity = 0;
        this.#isAnimating = false;
        this.#animatingSpringback = false;
    }

    get speed() {
        return this.#currentVelocity;
    }

    get isAnimating() {
        return this.#isAnimating;
    }

    get #tooSlow() {
        return Math.abs(this.#currentVelocity) < velocityCutoff;
    }

    get #isWithinSoftLimits() {
        return (
            this.#currentPosition <= this.#upperSoftLimit &&
            this.#currentPosition >= this.#lowerSoftLimit
        );
    }

    get #movingInwards() {
        return (
            (this.#currentPosition > this.#upperSoftLimit &&
                this.#currentVelocity < 0) ||
            (this.#currentPosition < this.#lowerSoftLimit &&
                this.#currentVelocity > 0)
        );
    }

    #updateVelocity() {
        if (this.#isWithinSoftLimits) {
            this.#currentVelocity /= innerFriction;
            if (this.#tooSlow) {
                this.stopAnimation();
            }
            return;
        }

        if (!this.#animatingSpringback && this.#movingInwards) {
            this.#currentVelocity /= innerFriction;
            return;
        }
        if (!this.#animatingSpringback) {
            this.#currentVelocity /= marginFriction;
            if (
                this.#currentPosition <= this.#lowerHardLimit ||
                this.#currentPosition >= this.#upperHardLimit
            )
                this.#startSpringback();
        } else {
            const offset =
                Math.min(this.#upperSoftLimit - this.#currentPosition, 0) +
                Math.max(this.#lowerSoftLimit - this.#currentPosition, 0);
            this.#currentVelocity = offset * 0.2;
        }
        if (this.#tooSlow) {
            if (this.#animatingSpringback) {
                this.stopAnimation();
            } else {
                this.#startSpringback();
            }
        }
    }

    #startSpringback() {
        this.#isAnimating = true;
        this.#animatingSpringback = true;
    }

    onFrame() {
        if (!this.#isAnimating) return;
        this.#scrollConstrained(this.#currentVelocity);
        this.#updateVelocity();
    }
}
