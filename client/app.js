import "./modules/docview/viewanimation.js";
import { toggleOverlay } from "./modules/gui/gui-actions.js";
import initGUI from "./modules/gui/gui-init.js";

initGUI();
toggleOverlay("newFileSetup");
