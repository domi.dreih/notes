import GeometryTool from "../abstracttools/geometrytool.js";

export default class StraightLineTool extends GeometryTool {
    constructor() {
        const createLine = (point) => {
            this.shape = new paper.Path.Line(point, point);
        };

        const updateLine = (point) => {
            this.shape.removeSegment(1);
            this.shape.add(point);
        };

        const handlers = {
            createShape: createLine,
            updateShape: updateLine,
        };
        super("straightLine", handlers);
    }
}
