import { $ } from "../../gui/dom-utils.js";
import BulkEditingTool from "../abstracttools/bulkeditingtool.js";
import toolStack from "../toolstack.js";

let isEraserButton = false;
$("#main").addEventListener("pointerdown", (e) => {
    isEraserButton = e.pointerType === "pen" && e.button === 5;
    if (isEraserButton) toolStack.activeTool = "erase";
});

const eraser = Object.preventExtensions({
    radius: 5,
    buffer: 10,
    position: {},
    touchesLine: function (p1, p2) {
        // s1 and s2 are the distances from the center point to the other points:
        const s1 = distance(this.position, p1);
        const s2 = distance(this.position, p2);
        if (s1 <= this.radius || s2 <= this.radius) return true;
        // if one other point is within the circle around the center, then return true
        else {
            const xDif = p1.x - p2.x;
            const yDif = p1.y - p2.y;
            const m = yDif / xDif; // m is the slope of the line through the points
            const negXCPerM = -(this.position.x / m);
            const perp1 = negXCPerM + p1.y + p1.x / m; // if the center point lies between the two lines, that are
            const perp2 = negXCPerM + p2.y + p2.x / m; // perpendicular to the main line and go through the main points, ...
            if (
                Math.min(perp1, perp2) <= this.position.y &&
                Math.max(perp1, perp2) >= this.position.y
            ) {
                // ... then do this:
                // distance is the minimum distance from the center point to the main line:
                const distance =
                    Math.abs(
                        -yDif * this.position.x -
                            -xDif * this.position.y +
                            p2.x * p1.y -
                            p2.y * p1.x
                    ) / Math.sqrt(yDif ** 2 + xDif, 2);
                if (distance <= this.radius) return true; // if this distance is not bigger than the radius, return true
            }
            return false;
        }
    },
    isBiggerThan: function (element) {
        return this.radius > element.width && this.radius > element.height;
    },
    _intersectionsWithLine: function (p1, p2) {
        const m = (p1.y - p2.y) / (p1.x - p2.x);
        const y0 = p1.y - m * p1.x; // m and y0 describe the line as y = (m * x) + y0
        const a = m ** 2 + 1;
        const b = 2 * m * (y0 - this.position.y) - 2 * this.position.x;
        const c =
            this.position.x ** 2 +
            (y0 - this.position.y) ** 2 -
            2 * this.radius; // a, b and c are coefficients of a quadratic equation a * x^2 + b * x + c = 0
        const solutions = solveQuadraticEquation(a, b, c); // the solutions of the equation tell the intersections of the line with the eraser
        return solutions.map((x) => {
            return { x: x, y: m * x + y0 };
        }); // converts the solutions to an array of points before returning
    },
    _nearestIntersectionTo: function (point, intersections) {
        return intersections[
            distance(point, intersections[0]) <
            distance(point, intersections[1])
                ? 0
                : 1
        ];
    },
    _shortenLine: function (fixedPoint, intersections) {
        const altered = this._nearestIntersectionTo(fixedPoint, intersections);
        return [[fixedPoint.x, fixedPoint.y, altered.x, altered.y]];
    },
    interactWithLine: function (p1, p2) {
        const linePoints = [p1, p2];
        const intersections = this._intersectionsWithLine(p1, p2);
        if (intersections.length < 2) return []; // return if the line does not have 2 intersections with the eraser
        if (distance(this.position, p1) <= this.radius) {
            return this._shortenLine(p2, intersections);
        }
        if (distance(this.position, p2) <= this.radius) {
            return this._shortenLine(p1, intersections);
        }
        const lengthOfLine = distance(...linePoints);
        let newLines = [];
        for (const point of linePoints) {
            if (distance(intersections[0], point) >= lengthOfLine) return [];
            const newPoint = this._nearestIntersectionTo(point, intersections);
            newLines.push([point.x, point.y, newPoint.x, newPoint.y]);
        }
        return newLines;
    },
});

export default class EraseTool extends BulkEditingTool {
    static eraseMode = "entireLine";
    #hasMadeModification = false;

    constructor() {
        const onMouseDown = () => {
            this.createCache();
        };

        const onMouseDrag = (event) => {
            const pointer = event.point;
            eraser.position = pointer;
            const foundHitResults = this.page.content.group.hitTestAll(pointer);
            foundHitResults.forEach((hitResult) => {
                if (EraseTool.#erasesEntireLine) {
                    this.page.content.remove(hitResult.item);
                    this.#hasMadeModification = true;
                } else {
                    // not yet converted to paper.js
                }
            });
        };

        const onMouseUp = () => {
            if (this.#hasMadeModification) this.finishModification();
            this.#hasMadeModification = false;
            if (isEraserButton) toolStack.activatePrevious();
        };

        const handlers = {
            onMouseDown,
            onMouseDrag,
            onMouseUp,
        };
        super("erase", "erase", handlers);
    }

    static get #erasesEntireLine() {
        return EraseTool.eraseMode === "entireLine";
    }
}
