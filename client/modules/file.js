import { lowerScope, upperScope } from "./docview/papersetup.js";
import { $ } from "./gui/dom-utils.js";
import history from "./history/history.js";
import {
    addPage,
    clearPages,
    initiatePage,
    serializePages,
} from "./pages/pagemanager.js";
import { clearPDFData, getPDFData, setPDFData } from "./pdf-rendering.js";

const socket = io("localhost:2480");

let filePath = "";

socket.on("getSerializedContent", (callback) => {
    const serializedContent = serializeContent({ contentJSON: true });
    callback(serializedContent);
});

socket.on("fileToBeOpened", (json) => {
    loadFromJSON(json);
});

socket.on("setCurrentState", (newState) => {
    filePath = newState?.filePath ?? filePath;
    if (newState?.allChangesSaved) history.setSavedModification();
});

socket.on("getCurrentState", (callback) => {
    callback({ filePath, allChangesSaved: history.isCurrentStateSaved });
});

/**
 *
 * @returns a Promise that resolves with a boolean indicating if the file setup dialog may be displayed
 */
function newFile() {
    return new Promise((resolve) => {
        socket.emit("confirmAllSaved", (continues) => {
            if (continues) clearFile();
            resolve(continues);
        });
    });
}

/**
 *
 * @returns a Promise that resolves with a boolean indicating if opening a file was successful
 */
function openFile() {
    return new Promise((resolve) => {
        socket.emit("openDialog", async (response) => {
            if (typeof response !== "object" || !response)
                return resolve(false);
            try {
                await loadFromJSON(response);
                resolve(true);
            } catch (error) {
                resolve(false);
            }
        });
    });
}

function saveFile() {
    socket.emit("save");
}

function saveFileAs() {
    socket.emit("saveAsDialog");
}

export const fileEventHandlers = {
    newFile,
    openFile,
    saveFile,
    saveFileAs,
    exportPDF,
};

function serializeContent(options) {
    return {
        pages: serializePages(options),
        path: filePath,
        pdfData: getPDFData(),
    };
}

function clearFile() {
    lowerScope.project.clear();
    upperScope.project.clear();
    history.clear();
    clearPages();
    clearPDFData();
}

function onFileLoaded() {
    $("#main").dispatchEvent(new CustomEvent("file-loaded"));
}

export function finishNewFilePreparation() {
    filePath = "";
    onFileLoaded();
}

export async function beginNewFile(template) {
    clearFile();
    await addPage(template);
    finishNewFilePreparation();
}

async function loadFromJSON(json) {
    try {
        clearFile();
        if (typeof json.content === "string") {
            // legacy file parser
            json.pages = json.pages.map((oldPage) => {
                const { number, top, left, width, height } = oldPage;
                const {
                    _bgColor: backgroundColor,
                    _ruling: ruling,
                    rulingColor,
                } = oldPage;
                const template = {
                    width,
                    height,
                    backgroundColor,
                    ruling,
                    rulingColor,
                };
                const contentJSON = oldPage.contentGroup;
                return { number, top, left, template, contentJSON };
            });
        }
        setPDFData(json.pdfData);
        filePath = json.path;
        for (const parameters of json.pages) {
            if (typeof parameters.template?.rulingParameters !== "object")
                parameters.template.rulingParameters =
                    getLegacyRulingParameters(parameters.template.ruling);
            await initiatePage(parameters);
        }
        onFileLoaded();
    } catch (e) {
        console.error(
            `An error ocurred processing the file "${json.path}":\n${e}`
        );
    }
}

function getLegacyRulingParameters(ruling) {
    const legacyCmToPx = (x) => (x * 96) / 2.54;
    switch (ruling) {
        case "lined": {
            const spacing = legacyCmToPx(Math.SQRT2 / 32);
            return { spacing, offset: spacing };
        }
        case "squared":
        case "dotted": {
            const spacing = legacyCmToPx(0.5);
            return { spacing, offset: spacing / 2 };
        }
        case "noLines":
        default:
            return {};
    }
}

function exportPDF({ includeRuling, includePDFBackground }) {
    socket.emit(
        "exportPDF",
        serializeContent({ contentSVG: true, rulingSVG: includeRuling }),
        { includeRuling, includePDFBackground }
    );
}
