import { $ } from "../gui/dom-utils.js";

const pointerManager = Object.preventExtensions({
    activePointerEvents: new Map(),
    gestureOrigin: new paper.Point(0, 0),
    gestureStartTime: 0,
    previousCenter: new paper.Point(0, 0),
    previousDistance: 0,
    previousTimestamp: 0,
    addPointer(event) {
        this.updatePointer(event);
        this.gestureOrigin = this.centerOfPointers;
        this.gestureStartTime = event.timeStamp;
    },
    updatePointer(event) {
        this.updatePreviousValues();
        this.previousTimestamp = event.timeStamp;
        this.activePointerEvents.set(event.pointerId, event);
        this.updateSwipeDetector(event);
    },
    removePointer(event) {
        this.activePointerEvents.delete(event.pointerId);
        this.gestureOrigin = this.centerOfPointers;
        this.gestureStartTime = event.timeStamp;
        this.updatePreviousValues();
        if (this.pointersCount === 0) swipeDetector.detectSwipe();
    },
    updatePreviousValues() {
        this.previousCenter = this.centerOfPointers;
        this.previousDistance = this.totalDistanceFromCenter;
    },
    updateSwipeDetector({ timeStamp }) {
        if (this.pointersCount === 1) {
            const { x, y } = this.centerOfPointers;
            swipeDetector.currentPosition = { x, y, timeStamp };
        }
    },
    get pointerArray() {
        return Array.from(this.activePointerEvents.values());
    },
    get centerOfPointers() {
        const sumOfPoints = this.pointerArray.reduce(
            (sum, current) => sum.add(current),
            new paper.Point(0, 0)
        );
        return sumOfPoints.divide(this.pointersCount);
    },
    get pointersCount() {
        return this.activePointerEvents.size;
    },
    get totalDistanceFromCenter() {
        const center = this.centerOfPointers;
        return this.pointerArray.reduce(
            (sum, current) => sum + center.getDistance(current),
            0
        );
    },
});

const SAMPLE_SIZE = 5;
const MIN_SWIPE_SPEED = 0.25;
const swipeDetector = Object.preventExtensions({
    latestPositions: [],
    /**
     * @param {{x: number, y: number, timeStamp: DOMHighResTimeStamp}} position
     */
    set currentPosition(position) {
        if (this.latestPositions.length >= SAMPLE_SIZE)
            this.latestPositions.shift();
        this.latestPositions.push(position);
    },
    get first() {
        return this.latestPositions[0];
    },
    get last() {
        return this.latestPositions[this.latestPositions.length - 1];
    },
    get averageDirection() {
        return new paper.Point(
            this.last.x - this.first.x,
            this.last.y - this.first.y
        ).divide(this.latestPositions.length);
    },
    get timeDifference() {
        return this.last.timeStamp - this.first.timeStamp;
    },
    detectSwipe() {
        if (this.latestPositions.length >= SAMPLE_SIZE) {
            const direction = this.averageDirection;
            const averageVelocity = direction.divide(this.timeDifference);
            if (averageVelocity.length >= MIN_SWIPE_SPEED)
                main.dispatchEvent(
                    new CustomEvent("swipe", { detail: { direction } })
                );
        }
        this.latestPositions = [];
    },
});

const main = $("#main");
main.addEventListener("pointerdown", (e) => {
    if (e.pointerType !== "touch") return;
    pointerManager.addPointer(e);
});

main.addEventListener("pointermove", (e) => {
    if (e.pointerType !== "touch") return;
    pointerManager.updatePointer(e);

    const oldPosition = pointerManager.previousCenter;
    const newPosition = pointerManager.centerOfPointers;
    main.dispatchEvent(
        new CustomEvent("pan", { detail: { oldPosition, newPosition } })
    );
    if (pointerManager.pointersCount < 2) return;

    const oldDistance = pointerManager.previousDistance;
    const newDistance = pointerManager.totalDistanceFromCenter;
    main.dispatchEvent(
        new CustomEvent("pinch", {
            detail: { oldDistance, newDistance, center: newPosition },
        })
    );
});

main.addEventListener("pointerup", (e) => {
    if (e.pointerType !== "touch") return;
    pointerManager.removePointer(e);
    if (pointerManager.pointersCount === 0)
        main.dispatchEvent(new CustomEvent("all-pointers-up"));
});
