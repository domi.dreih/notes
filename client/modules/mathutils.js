const CM_TO_PT = 72 / 2.54; // multiply any length in centimeters with this to convert it to points

export function snapToGrid(point) {
    const gridUnit = 0.25 * CM_TO_PT;
    let newPoint = point.divide(gridUnit);
    newPoint.x = Math.round(newPoint.x);
    newPoint.y = Math.round(newPoint.y);
    return newPoint.multiply(gridUnit);
}

export function distance(p1, p2) {
    return Math.sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2);
}

export function solveQuadraticEquation(a, b, c) {
    // solves any quadratic equation of the form a * x^2 + b * x + c = 0 ("Mitternachtsformel")
    const radicant = b ** 2 - 4 * a * c;
    if (radicant < 0) return [];
    const x1 = (-b + Math.sqrt(radicant)) / (2 * a);
    if (radicant === 0) return [x1];
    const x2 = (-b - Math.sqrt(radicant)) / (2 * a);
    return [x1, x2];
}

export function cm_to_pt(centimeters) {
    return centimeters * CM_TO_PT;
}
