import BulkModification from "../../history/bulkmodification.js";
import history from "../../history/history.js";
import Modification from "../../history/modification.js";
import { getContentCache, getPageFromIndex } from "../../pages/pagemanager.js";
import EditingTool from "./editingtool.js";

export default class BulkEditingTool extends EditingTool {
    #cachedContent;

    constructor(name, type, handlers) {
        super(name, type, handlers);
    }

    createCache() {
        this.#cachedContent = getContentCache();
    }

    finishModification() {
        const newContent = getContentCache();
        const modifications = [];
        for (let i = 0; i < this.#cachedContent.length; i++) {
            const oldJSON = this.#cachedContent[i];
            const newJSON = newContent[i];
            if (oldJSON === newJSON) continue;
            const page = getPageFromIndex(i);
            const undo = () => {
                page.content.deserialize(oldJSON);
            };
            const redo = () => {
                page.content.deserialize(newJSON);
            };
            const modification = new Modification({ undo, redo });
            modifications.push(modification);
        }
        history.add(new BulkModification(modifications));
    }
}
