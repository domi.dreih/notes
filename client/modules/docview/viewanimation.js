import { $ } from "../gui/dom-utils.js";
import {
    getDocumentBounds,
    getPageFromIndex,
    updateAllPages,
} from "../pages/pagemanager.js";
import "./gesturedetection.js";
import { lowerScope, upperScope } from "./papersetup.js";
import ScrollDimension from "./scrolldimension.js";
import { rectangleToView, scale, scroll } from "./viewtransforms.js";

let xDimension = new ScrollDimension();
let yDimension = new ScrollDimension();
const main = $("#main");

let animationTimeout;
const refreshAnimationTimeout = () => {
    clearTimeout(animationTimeout);
    animationTimeout = setTimeout(startScrollAnimations, 200);
};

main.addEventListener("wheel", (e) => {
    e.preventDefault();
    if (e.ctrlKey) {
        const factor = 1.001 ** -e.deltaY;
        const origin = lowerScope.view.getEventPoint(e);
        scaleConstrained(factor);
        const newPos = lowerScope.view.getEventPoint(e);
        scrollConstrained(newPos.subtract(origin));
    } else {
        const delta = -e.deltaY / 5;
        scrollConstrained(
            new paper.Point(...(e.shiftKey ? [delta, 0] : [0, delta]))
        );
    }
    if (e.deltaX) scrollConstrained(new paper.Point(-e.deltaX / 5, 0));
    refreshAnimationTimeout();
});

main.addEventListener("pointerdown", () => {
    xDimension.stopAnimation();
    yDimension.stopAnimation();
});

const SNAPPING_ANGLE = 30;
const snapToCardinalDirection = ({ x, y }) => {
    const radius = Math.hypot(x, y);
    const angle = (Math.atan2(y, x) * 180) / Math.PI;
    if (Math.abs(angle) < SNAPPING_ANGLE) return { x: radius, y: 0 };
    if (Math.abs(angle - 90) < SNAPPING_ANGLE) return { x: 0, y: radius };
    if (Math.abs(angle - 180) < SNAPPING_ANGLE) return { x: -radius, y: 0 };
    if (Math.abs(angle - 270) < SNAPPING_ANGLE) return { x: 0, y: -radius };
    return { x, y };
};
main.addEventListener("pan", (e) => {
    const oldPosition = upperScope.view.viewToProject(e.detail.oldPosition);
    const newPosition = upperScope.view.viewToProject(e.detail.newPosition);
    const rawDelta = newPosition.subtract(oldPosition);
    scrollConstrained(snapToCardinalDirection(rawDelta));
});

main.addEventListener("pinch", (e) => {
    const oldCenter = upperScope.view.viewToProject(e.detail.center);
    const { oldDistance, newDistance } = e.detail;
    const scaleFactor = newDistance / oldDistance;
    scaleConstrained(scaleFactor);
    const newCenter = upperScope.view.viewToProject(e.detail.center);
    const direction = newCenter.subtract(oldCenter);
    scrollConstrained(direction);
});

const boost = (speed) => speed + 0.005 * speed * Math.abs(speed);
main.addEventListener("swipe", (e) => {
    const { x, y } = snapToCardinalDirection(e.detail.direction);
    xDimension.inertia = x;
    yDimension.inertia = boost(y);
});

main.addEventListener("pointermove", function (e) {
    if (e.pointerType === "pen") {
        xDimension.stopAnimation();
        yDimension.stopAnimation();
    }
});

function startScrollAnimations() {
    xDimension.startAnimation();
    yDimension.startAnimation();
    if (!xDimension.isAnimating && !yDimension.isAnimating) updateAllPages();
}

main.addEventListener("all-pointers-up", startScrollAnimations);

main.addEventListener("page-operation", () => {
    const pageSize = getDocumentBounds();
    xDimension.pageSize = pageSize.width;
    yDimension.pageSize = pageSize.height;
});

upperScope.view.on("resize", () => {
    const viewSize = upperScope.view.size;
    xDimension.setViewSize(viewSize.width, true);
    yDimension.setViewSize(viewSize.height, true);
    startScrollAnimations();
});

upperScope.view.onFrame = () => {
    const isAnimatingBefore = xDimension.isAnimating || yDimension.isAnimating;
    if (!isAnimatingBefore) return;
    xDimension.onFrame();
    yDimension.onFrame();
    const isAnimatingAfter = xDimension.isAnimating || yDimension.isAnimating;
    if (!isAnimatingAfter) updateAllPages();
};

main.addEventListener("file-loaded", resetView);
function resetView() {
    const pageSize = getDocumentBounds();
    centerInView(pageSize.center);
    const firstPage = getPageFromIndex(0);
    scaleViewToWidth(firstPage);
    const viewSize = upperScope.view.size;
    xDimension = new ScrollDimension(
        pageSize.width,
        viewSize.width,
        (deltaX) => {
            scroll({ x: deltaX });
        }
    );
    yDimension = new ScrollDimension(
        pageSize.height,
        viewSize.height,
        (deltaY) => {
            scroll({ y: deltaY });
        }
    );
    jumpToTopOfPage(firstPage);
    setTimeout(updateAllPages, 100);
}

function centerInView(point) {
    lowerScope.view.center = point;
    upperScope.view.center = point;
}

function scaleViewToWidth(page) {
    const viewSize = upperScope.view.viewSize;
    const pageInView = rectangleToView(page);
    const scaleAdjust = (0.9 * viewSize.width) / pageInView.width;
    scaleConstrained(scaleAdjust);
}

function jumpToTopOfPage(page) {
    const viewSize = upperScope.view.viewSize;
    const pageAnchor = page.topCenter;
    const viewAnchor = upperScope.view.viewToProject(
        new paper.Point(viewSize.width / 2, 120)
    );
    scrollConstrained(viewAnchor.subtract(pageAnchor));
}

const MIN_ZOOM = 0.5;
const MAX_ZOOM = 5;
function scaleConstrained(factor) {
    const currentZoom = upperScope.view.zoom;
    const predictedZoom = currentZoom * factor;
    if (predictedZoom <= MIN_ZOOM) factor = MIN_ZOOM / currentZoom;
    if (predictedZoom >= MAX_ZOOM) factor = MAX_ZOOM / currentZoom;
    scale(factor);

    const viewSize = upperScope.view.size;
    xDimension.setViewSize(viewSize.width);
    yDimension.setViewSize(viewSize.height);
}

function scrollConstrained(delta) {
    xDimension.handleScrollInput(delta.x);
    yDimension.handleScrollInput(delta.y);
}
