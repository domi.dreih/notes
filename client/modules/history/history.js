const history = Object.preventExtensions({
    maxLen: 70,
    _undoHist: [],
    _redoHist: [],
    _savedModification: undefined,
    get undoPossible() {
        return this._undoHist.length > 0;
    },
    get redoPossible() {
        return this._redoHist.length > 0;
    },
    add: function (modification) {
        if (this.redoPossible) this._redoHist = [];
        if (this._undoHist.length >= this.maxLen) this._undoHist.pop();
        this._undoHist.unshift(modification);
        return this;
    },
    undo: function () {
        if (!this.undoPossible) return;
        const modification = this._undoHist.shift();
        modification.undo();
        this._redoHist.unshift(modification);
        return this;
    },
    redo: function () {
        if (!this.redoPossible) return;
        const modification = this._redoHist.shift();
        modification.redo();
        this._undoHist.unshift(modification);
        return this;
    },
    clear: function () {
        this._undoHist = [];
        this._redoHist = [];
        this._savedModification = undefined;
        return this;
    },
    setSavedModification: function () {
        this._savedModification = this._undoHist[0];
    },
    get isCurrentStateSaved() {
        return this._savedModification === this._undoHist[0];
    },
});

export default history;
