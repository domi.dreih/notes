import { fileEventHandlers, beginNewFile } from "../../file.js";
import { cm_to_pt } from "../../mathutils.js";
import { importPDF } from "../../pdf-insertion.js";
import { $, $each } from "../dom-utils.js";
import { toggleOverlay } from "../gui-actions.js";

const A4_DIMENSIONS = {
    width: cm_to_pt(21),
    height: cm_to_pt(21 * Math.SQRT2),
};

$each("button.templateOption", (input) => {
    const ruling = input.value;
    input.addEventListener("click", () => {
        toggleOverlay("newFileSetup");
        beginNewFile({ ...A4_DIMENSIONS, ruling });
    });
});

$("button[value='openFile']").addEventListener("click", async () => {
    const continues = await fileEventHandlers.openFile();
    if (continues) toggleOverlay("newFileSetup");
});
$("button[value='importPDF']").addEventListener("click", async () => {
    await importPDF();
    toggleOverlay("newFileSetup");
});
